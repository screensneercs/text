
git:
	@git config user.name "screens"
	@git config user.email "screensneercs@gmail.com"

newpost: git
	touch ./"$$(date +%Y-%m-%dT%H:%M).md"
	ls -lat ./"$$(date +%Y-%m-%dT%H:%M).md"

push: git
	git add .
	git commit -m "Updates"
	git push origin master
